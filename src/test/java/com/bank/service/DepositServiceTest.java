package com.bank.service;

import com.bank.dto.deposit.DepositRequest;
import com.bank.dto.deposit.DepositResponse;
import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.entitiy.MetodDeposit;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.DepositRepository;
import com.bank.repository.MethodDepositRepository;
import com.bank.util.KodeDeposit;
import com.bank.errorhandler.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Optional;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DepositServiceTest {
    @InjectMocks
    private DepositService depositService;
    @Mock
    private DepositRepository depositRepository;
    @Mock
    private MethodDepositRepository methodDepositRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private KodeDeposit kodeDeposit;
    @Mock
    private Validation validation;

    @Test
    void doPositifAddDeposit()  {
        var c = new Customer();
        c.setId(1);
        c.setNama("nico");
        c.setToken("rre");

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening("1234");
        akun.setSaldo(BigDecimal.ZERO);
        akun.setPin("12");
        akun.setCustomer(c);

        var metode = new MetodDeposit();
        metode.setId(1);
        metode.setKeterangan("atm");
        metode.setKodeMetod("11");

        String valid = "rre";
        String kodeDeposit1 = "KodeDeposit";

        var request = new DepositRequest();
        request.setMetode("atm");
        request.setNominal(BigDecimal.valueOf(50000));

        when(methodDepositRepository.findByKeterangan(request.getMetode())).thenReturn(Optional.of(metode));

        validation.validateToken(akun.getId(), valid);
        when(accountRepository.findById(1)).thenReturn(Optional.of(akun));
        when(kodeDeposit.getKode(1, request)).thenReturn(kodeDeposit1);

        DepositResponse response = depositService.addDeposit(1, request, "rre");

        Assertions.assertEquals(response.getKodeMetod(), kodeDeposit1);
    }
    @Test
    void doNegatifAddDeposit() {
        Integer id = 1;
        String invalid = "rre";
        validation.validateToken(id,invalid);

        Mockito.lenient().when(depositRepository.findAllByAccountId(id)).thenReturn(null);
        Assertions.assertThrows(CustomerNotExistsException.class, ()->
                depositService.addDeposit(id, new DepositRequest(), invalid));
    }
}