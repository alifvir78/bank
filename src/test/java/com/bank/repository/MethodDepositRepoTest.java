package com.bank.repository;

import com.bank.entitiy.MetodDeposit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Date;
import java.util.Optional;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class MethodDepositRepoTest {
    @Mock
    private MethodDepositRepository methodDepositRepository;

    public MetodDeposit metodDeposit(){
        var md = new MetodDeposit();
        md.setId(1);
        md.setKeterangan("atm");
        md.setKodeMetod("11");
        md.setLastUpdate(new Date(2023/12/3));
        return md;
    }
    @Test
    void doPositifMethodDeposit(){
        when(methodDepositRepository.findByKeterangan("atm"))
                .thenReturn(Optional.of(metodDeposit()));
        Optional<MetodDeposit> expect = methodDepositRepository.findByKeterangan("atm");
        Assertions.assertTrue(expect.isPresent());
        Assertions.assertEquals(metodDeposit(), expect.get());
    }
    @Test
    void doNegatifMethodDeposit(){
        String keterangan = "market";
        when(methodDepositRepository.findByKeterangan(keterangan))
                .thenReturn(Optional.empty());
        Optional<MetodDeposit> expect = methodDepositRepository.findByKeterangan(keterangan);
        Assertions.assertTrue(expect.isEmpty());
        verify(methodDepositRepository).findByKeterangan(keterangan);
    }
}
