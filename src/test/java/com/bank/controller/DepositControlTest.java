package com.bank.controller;

import com.bank.dto.deposit.DepositRequest;
import com.bank.dto.deposit.DepositResponse;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.service.DepositService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.math.BigDecimal;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DepositControlTest {
    @InjectMocks
    private DepositController depositController;
    @Mock
    private DepositService depositService;

    @Test
    void doPositifDepositControllAdd(){
        Integer accountId = 1;
        var request = new DepositRequest();
        request.setMetode("atm");
        request.setNominal(BigDecimal.valueOf(50000));

        String token = "valid";

        var response = DepositResponse.builder()
                .kodeMetod("kodeMethod")
                .build();

        when(depositService.addDeposit(accountId, request, token))
                .thenReturn(response);
        ResponseEntity<DepositResponse> responseEntity = depositController.addDeposit(token, accountId, request);
        Assertions.assertSame(HttpStatus.CREATED, responseEntity.getStatusCode());
    }
    @Test
    void doNegatifDepositControllAdd()  {
        Integer id = 99;
        var request = new DepositRequest();
        request.setMetode("atm");
        request.setNominal(BigDecimal.valueOf(50000));

        String token = "token";
        doThrow(new CustomerNotExistsException("Customer Not Found"))
                .when(depositService).addDeposit(id, request, token);
        Assertions.assertThrows(CustomerNotExistsException.class, () ->
                depositController.addDeposit(token, id, request));
    }
}
