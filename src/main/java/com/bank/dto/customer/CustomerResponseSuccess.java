package com.bank.dto.customer;

import lombok.*;
import java.math.BigDecimal;

@Data
@Builder
public class CustomerResponseSuccess {
    private String message;
    private Integer id;
    private String nama;
    private String noHp;
    private String email;
    private String alamat;
    private String username;
    private String nomerRekening;
    private BigDecimal saldo;

}
