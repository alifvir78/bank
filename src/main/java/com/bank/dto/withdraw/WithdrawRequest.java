package com.bank.dto.withdraw;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class WithdrawRequest {
    private BigDecimal totalWithdraw;
    private String pin;
}