package com.bank.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GenerateNoTransaksi {
    private GenerateNoTransaksi(){
        throw new IllegalArgumentException("This Util Error");
    }

    public static String generateNomorTransaksi(){
        var currentDate = new Date();

        var dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");

        String formattedDate = dateFormat.format(currentDate);

        return String.format("%14s", formattedDate).replace(' ', '0');
    }

}


