package com.bank.dto.transfer;

import lombok.Data;
import java.math.BigDecimal;

@Data
public class TransferRequest {
    private String noRekeningTujuan;
    private BigDecimal totalTransfer;
    private String pin;
}
