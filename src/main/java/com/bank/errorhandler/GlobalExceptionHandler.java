package com.bank.errorhandler;


import com.bank.exception.ErrorHandlerException;
import com.bank.exception.customer.CustomerNotExistsException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomerNotExistsException.class)
    public ResponseEntity<ErrorResponse> handleCustomerNotFoundException(CustomerNotExistsException ex) {
        var errorResponse = ex.errorResponseMessage();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ErrorHandlerException.class)
    public ResponseEntity<ErrorRegistResponse> handleExceptions(Exception ex) {
        var httpStatus = HttpStatus.BAD_REQUEST;

        var errorResponse = new ErrorRegistResponse(
                new Date(),
                httpStatus.value(),
                httpStatus,
                ex.getMessage());

        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, List<String>>> handleValidationException(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        Map<String, List<String>> result = new HashMap<>();
        result.put("errors", errors);
        return ResponseEntity.badRequest().body(result);
    }
}
