package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.dto.deposit.DepositRequest;
import com.bank.dto.deposit.DepositResponse;
import com.bank.entitiy.Account;
import com.bank.entitiy.Deposit;
import com.bank.entitiy.MetodDeposit;
import com.bank.exception.ErrorHandlerException;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.DepositRepository;
import com.bank.errorhandler.Validation;
import com.bank.repository.MethodDepositRepository;
import com.bank.util.GenerateNoTransaksi;
import com.bank.util.KodeDeposit;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DepositService {
    private final AccountRepository accountRepository;
    private final DepositRepository depositRepository;
    private final MethodDepositRepository methodDepositRepository;
    private final KodeDeposit kodeDeposit;
    private final Validation validation;

    public DepositResponse addDeposit(Integer id, DepositRequest request,String token) {
        validation.validateCustomerNotExists(id);
        validation.validateToken(id,token);
        validation.validateMetodeCannotBlank(request.getMetode());
        validation.validateNominalDeposit(request.getNominal());
        Optional<Account> accountOptional = accountRepository.findById(id);
        String kode = kodeDeposit.getKode(id, request);


        var account = accountOptional.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN)) ;
        account.setSaldo(account.getSaldo().add(request.getNominal()));
        account.setLastUpdate(new Date());
        accountRepository.save(account);

        Optional<MetodDeposit> metodDepositOptional = methodDepositRepository.findByKeterangan(request.getMetode());
        validation.validateMetodeDeposit(request.getMetode());
        var metodeDeposit = metodDepositOptional.orElseThrow(()-> new ErrorHandlerException(ConstansMessage.METODE_EMPTY));

        var deposit = new Deposit();
        deposit.setTotalDeposit(request.getNominal());
        deposit.setNoTransaksi(GenerateNoTransaksi.generateNomorTransaksi());
        deposit.setDepositDate(new Date());
        deposit.setLastUpdate(new Date());
        deposit.setAccount(account);
        deposit.setMetode(request.getMetode());
        deposit.setMetodDeposit(metodeDeposit);
        depositRepository.save(deposit);

        return DepositResponse.builder()
                .kodeMetod(kode)
                .build();
    }
}
