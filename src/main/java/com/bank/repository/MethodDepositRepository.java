package com.bank.repository;

import com.bank.entitiy.MetodDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface MethodDepositRepository extends JpaRepository<MetodDeposit, Integer> {
    Optional<MetodDeposit> findByKeterangan(String metode);
}
