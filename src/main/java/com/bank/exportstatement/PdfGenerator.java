package com.bank.exportstatement;

import com.bank.dto.statement.AccountStatement;
import com.bank.dto.statement.StatementList;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;
import org.springframework.stereotype.Service;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PdfGenerator {

    private static final Logger logger = Logger.getLogger(PdfGenerator.class.getName());

    private PdfGenerator(){

    }

    public static byte[] generatePdf(AccountStatement accountStatement){
        try (var outputStream = new ByteArrayOutputStream();
             var fileOutputStream = new FileOutputStream("statement.pdf")){
            var document = new Document(PageSize.LEGAL.rotate());
            PdfWriter.getInstance(document, outputStream);

            document.open();

            document.add(new Paragraph("Nama                  : " + accountStatement.getNama()));
            document.add(new Paragraph("Nomor Rekening : " + accountStatement.getNoRekening()));
            document.add(new Paragraph("Saldo                   : " + accountStatement.getSaldo()));
            document.add(new Paragraph(" "));

            var table = new PdfPTable(6);
            table.setWidthPercentage(100);
            table.setSpacingBefore(10);
            table.addCell(getBold("Tipe Transaksi"));
            table.addCell(getBold("Nomor Transaksi"));
            table.addCell(getBold("Tanggal Transaksi"));
            table.addCell(getBold("Kredit"));
            table.addCell(getBold("Debit"));
            table.addCell(getBold("Rekening Tujuan"));

            for (StatementList statementList : accountStatement.getStatementLists()){
                table.addCell(statementList.getTipeTransaksi());
                table.addCell(statementList.getNomorTransaksi());
                table.addCell(statementList.getTanggalTransaksi().toString());
                table.addCell(statementList.getKredit().toString());
                table.addCell(statementList.getDebit().toString());
                table.addCell(statementList.getRekeningTujuan());
            }
            document.add(table);
            document.close();

            var pdfbytes = outputStream.toByteArray();
            fileOutputStream.write(pdfbytes);

            return outputStream.toByteArray();
        }catch (DocumentException | IOException e){
            logger.log(Level.SEVERE, "Generate Failed", e);
            return new byte[0];
        }
    }

    private static PdfPCell getBold(String content){
        var cell = new PdfPCell(new Phrase(content, FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(BaseColor.CYAN);
        return cell;
    }
}
