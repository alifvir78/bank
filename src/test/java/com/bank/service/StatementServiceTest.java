package com.bank.service;

import com.bank.dto.statement.AccountStatement;
import com.bank.entitiy.*;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.DepositRepository;
import com.bank.repository.TransferRepository;
import com.bank.repository.WithdrawRepository;
import com.bank.errorhandler.Validation;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StatementServiceTest {
    @Mock
    private DepositRepository depositRepository;
    @Mock
    private WithdrawRepository withdrawRepository;
    @Mock
    private TransferRepository transferRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private Validation validation;
    @InjectMocks
    private StatementService statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);

    public Customer nasabah(){
        var c = new Customer();
        c.setId(1);
        c.setNama("nico");
        c.setEmail("email");
        c.setAlamat("bekasi");
        c.setNoHp("021");
        c.setUsername("nico");
        c.setPassword("nicopas");
        c.setToken("token");
        c.setLastUpdate(new Date());
        return c;
    }

    public Account akun(){
        var account = new Account();
        account.setId(1);
        account.setCustomer(nasabah());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening("123456");
        account.setPin("11");
        account.setLastUpdate(new Date());
        return account;
    }

    public MetodDeposit metodDeposit(){
        var md = new MetodDeposit();
        md.setId(1);
        md.setKeterangan("atm");
        md.setKodeMetod("11");
        return md;
    }

    public Deposit deposit(){
        var d = new Deposit();
        d.setAccount(akun());
        d.setId(1);
        d.setNoTransaksi("125678");
        d.setTotalDeposit(BigDecimal.valueOf(100000));
        d.setMetode(metodDeposit().getKeterangan());
        d.setMetodDeposit(metodDeposit());
        d.setDepositDate(new Date());
        return d;
    }

    public Withdraw withdraw(){
        var w = new Withdraw();
        w.setId(1);
        w.setAccount(akun());
        w.setNoTransaksi("32134342");
        w.setTotalWithdraw(BigDecimal.valueOf(50000));
        w.setWithdrawDate(new Date());
        return w;
    }

    public Transfer transfer(){
        var t = new Transfer();
        t.setId(1);
        t.setAccount(akun());
        t.setNoTransaksi("6721273");
        t.setRekeningTujuan("dummy No Rek");
        t.setTotalTransfer(BigDecimal.valueOf(25000));
        t.setTransferDate(new Date());
        return t;
    }

    @SneakyThrows
    @Test
    void doPositifGetAll(){
        Integer id = 1;
        String token = "token";

        when(accountRepository.findById(akun().getId())).thenReturn(Optional.of(akun()));
        when(depositRepository.findAllByAccountId(akun().getId())).thenReturn(Collections.singletonList(deposit()));
        when(withdrawRepository.findAllByAccountId(akun().getId())).thenReturn(Collections.singletonList(withdraw()));
        when(transferRepository.findAllByAccountId(akun().getId())).thenReturn(Collections.singletonList(transfer()));

        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);
        AccountStatement result = statementService.getAll(id, token);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(3, result.getStatementLists().size());
    }

    @Test
    void doNegatifGetAll()throws CustomerNotExistsException{
        Integer id = 99;
        String token = "invalidToken";

        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);

        Assertions.assertThrows(CustomerNotExistsException.class, ()->statementService.getAll(id, token));
    }
    @SneakyThrows
    @Test
    void doPositifGetDeposit(){
        Integer id = 1;
        String token = "Token";

        when(accountRepository.findById(akun().getId())).thenReturn(Optional.of(akun()));
        when(depositRepository.findAllByAccountId(akun().getId())).thenReturn(Collections.singletonList(deposit()));

        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);
        AccountStatement result = statementService.getDeposit(id, token);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1, result.getStatementLists().size());


    }
    @SneakyThrows
    @Test
    void doNegatifGetDeposit(){
        Integer id = 99;
        String token = "invalidToken";

        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);

        Assertions.assertThrows(CustomerNotExistsException.class, ()->statementService.getDeposit(id, token));
    }
    @SneakyThrows
    @Test
    void doPositifGetWithdraw(){
        Integer id = 1;
        String token = "token";

        when(accountRepository.findById(akun().getId())).thenReturn(Optional.of(akun()));
        when(withdrawRepository.findAllByAccountId(akun().getId())).thenReturn(Collections.singletonList(withdraw()));

        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);
        AccountStatement result = statementService.getWithdraw(id, token);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1, result.getStatementLists().size());
    }
    @SneakyThrows
    @Test
    void doNegatifGetWithdraw(){
        Integer id = 99;
        String token = "invalidToken";

        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);

        Assertions.assertThrows(CustomerNotExistsException.class, ()->statementService.getWithdraw(id, token));
    }
    @SneakyThrows
    @Test
    void doPositifGetTransfer(){
        Integer id = 1;
        String token = "token";

        when(accountRepository.findById(akun().getId())).thenReturn(Optional.of(akun()));
        when(transferRepository.findAllByAccountId(akun().getId())).thenReturn(Collections.singletonList(transfer()));

        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);
        AccountStatement result = statementService.getTransfer(id, token);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1, result.getStatementLists().size());
    }
    @SneakyThrows
    @Test
    void doNegatifGetTransfer(){
        Integer id = 99;
        String token = "invalidToken";

        when(accountRepository.findById(id)).thenReturn(Optional.empty());
        statementService = new StatementService(accountRepository, depositRepository, withdrawRepository, transferRepository, validation);

        Assertions.assertThrows(CustomerNotExistsException.class, ()->statementService.getTransfer(id, token));
    }
}
