package com.bank.controller;

import com.bank.dto.withdraw.WithdrawRequest;
import com.bank.dto.withdraw.WithdrawResponse;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.service.WithdrawService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.math.BigDecimal;
import java.util.Date;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WithdrawControllerTest {

    @Mock
    private WithdrawService withdrawService;

    @InjectMocks
    private WithdrawController withdrawController;

    @Test
    void doPositiveWithdrawControllerAdd(){
        Integer accountId = 1;
        WithdrawRequest request = new WithdrawRequest();
        request.setTotalWithdraw(BigDecimal.valueOf(100000));
        request.setPin("123456");

        String token =  "valid_token";

        WithdrawResponse response = WithdrawResponse.builder()
                .totalWithdraw(request.getTotalWithdraw())
                .withdrawDate(new Date())
                .kodeWithdraw("989898")
                .build();

        when(withdrawService.addWithdraw(accountId, request, token))
                .thenReturn(response);
        ResponseEntity<WithdrawResponse> responseEntity = withdrawController.addWithdraw(token, accountId, request);
        Assertions.assertSame(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    void doNegativeWithdrawControllerAdd(){
        Integer id = 99;
        WithdrawRequest request = new WithdrawRequest();
        request.setTotalWithdraw(BigDecimal.valueOf(1000000));

        String token = "token";
        doThrow(new CustomerNotExistsException("Customer Not Found"))
                .when(withdrawService).addWithdraw(id, request, token);
        Assertions.assertThrows(CustomerNotExistsException.class, () ->
                withdrawController.addWithdraw(token, id, request));
    }
}
