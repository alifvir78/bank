package com.bank.exception;

public class ErrorHandlerException extends RuntimeException{
    public ErrorHandlerException(String message){
        super(message);
    }
}
