package com.bank.dto.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class TransferResponse {
    private String message;
    private String noRekeningTujuan;
    private String noTransaksi;
    private BigDecimal totalTransfer;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "Asia/Jakarta")
    private Date timeTransfer;
}
