# Withdraw API Spec

## Customer Withdraw

Endpoint : POST /api/customers/withdraw/{customerId}

Request Body :

```json
{
    "totalWithdraw" : 1000000,
    "pin" : 123456
}
```

Response Body (Success) :

```json
{
  "withdrawDate": "14-12-2023 17:04:36",
  "noTransaksi": "20231214170436250",
  "totalWithdraw": 200000,
  "kodeWithdraw": "476898"
}
```

Response Body (Failed due to insufficient balance) :

```json
{
  "timestamp": "2023-12-14 09:55:13",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "Saldo Anda Kurang"
}
```

Response Body (Failed because of wrong PIN) :

```json
{
  "timestamp": "2023-12-14 09:59:37",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "Pin yang anda masukan salah"
}
```