package com.bank.dto.statement;

import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class StatementList {
    private String tipeTransaksi;
    private String nomorTransaksi;
    private Date tanggalTransaksi;
    private BigDecimal debit;
    private BigDecimal kredit;
    private String rekeningTujuan;
}
