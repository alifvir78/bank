package com.bank.controller;


import com.bank.dto.withdraw.WithdrawRequest;
import com.bank.dto.withdraw.WithdrawResponse;
import com.bank.service.WithdrawService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("bank/withdraw")
@RequiredArgsConstructor
public class WithdrawController {

    private final WithdrawService withdrawService;

    @PostMapping("/{id}")
    public ResponseEntity<WithdrawResponse> addWithdraw (@RequestHeader("token")String token, @PathVariable("id") Integer id,@RequestBody WithdrawRequest request){
        WithdrawResponse response =  withdrawService.addWithdraw(id, request,token);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}