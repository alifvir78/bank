package com.bank.service;

import com.bank.dto.login.LoginRequest;
import com.bank.dto.login.LoginResponse;
import com.bank.entitiy.Customer;
import com.bank.entitiy.Login;
import com.bank.errorhandler.Validation;
import com.bank.repository.CustomerRepository;
import com.bank.repository.LoginRepository;
import com.bank.util.JwtToken;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final CustomerRepository customerRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final LoginRepository loginRepository;

    private final Validation validation;

    public LoginResponse login(@NotNull LoginRequest request){
        Optional<Customer> optionalCustomer = customerRepository.findByUsername(request.getUsername());
        validation.validateUsername(request);
        var customer = optionalCustomer.get();
        var loginDate = new Login();
        validation.validatePassword(request);
        customer.setToken(JwtToken.getToken(request));
        customerRepository.save(customer);

        loginDate.setCustomer(customer);
        loginDate.setLastLogin(new Date());
        loginRepository.save(loginDate);

        return LoginResponse.builder()
                 .token(customer.getToken())
                .lastLogin(new Date())
                .build();
    }
}
