package com.bank.repository;

import com.bank.entitiy.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Date;
import java.util.Optional;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerRepositoryTest {
    @Mock
    private CustomerRepository customerRepository;

    public Customer nasabah(){
        var cust = new Customer();
        cust.setId(1);
        cust.setNama("syifa");
        cust.setEmail("email");
        cust.setAlamat("sukabumi");
        cust.setNoHp("021");
        cust.setUsername("syifnak");
        cust.setPassword("123456");
        cust.setToken("qwerty");
        cust.setLastUpdate(new Date());
        return cust;
    }

    @Test
    void doPositiveFindByUsername(){
        Customer customer = nasabah();

        when(customerRepository.findByUsername(customer.getUsername())).thenReturn(Optional.of(customer));

        Optional<Customer> response = customerRepository.findByUsername(customer.getUsername());

        Assertions.assertTrue(response.isPresent());
        Assertions.assertEquals(customer, response.get());
    }

    @Test
    void doNegativeFindByUsername(){
        String usernameNotFound = "sipa";

        when(customerRepository.findByUsername(usernameNotFound)).thenReturn(Optional.empty());

        Optional<Customer> response = customerRepository.findByUsername(usernameNotFound);

        Assertions.assertFalse(response.isPresent());
    }

    @Test
    void doPositiveExistByUsername(){
        String existUsername = "syifank";

        when(customerRepository.existsByUsername(existUsername)).thenReturn(true);

        boolean response = customerRepository.existsByUsername(existUsername);

        Assertions.assertTrue(response);
    }

    @Test
    void doNegativeExistByUsername(){
        String noExistUsername = "sipa";

        when(customerRepository.existsByUsername(noExistUsername)).thenReturn(false);

        boolean response = customerRepository.existsByUsername(noExistUsername);

        Assertions.assertFalse(response);
    }

    @Test
    void doPositiveExistByNoHP(){
        String noHpExist = "085219717446";

        when(customerRepository.existsByNoHp(noHpExist)).thenReturn(true);

        boolean response = customerRepository.existsByNoHp(noHpExist);

        Assertions.assertTrue(response);
    }

    @Test
    void doNegativeExistByNoHP(){
        String noExistNoHP = "085860671625";

        when(customerRepository.existsByNoHp(noExistNoHP)).thenReturn(false);

        boolean response = customerRepository.existsByNoHp(noExistNoHP);

        Assertions.assertFalse(response);
    }
}
