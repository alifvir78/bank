package com.bank.dto.customer;


import lombok.Builder;
import lombok.Data;
import java.math.BigDecimal;

@Data
@Builder
public class CustomerResponse {
    private Integer id;
    private String nama;
    private String noHp;
    private String email;
    private String alamat;
    private String username;
    private String nomerRekening;
    private BigDecimal saldo;



}
