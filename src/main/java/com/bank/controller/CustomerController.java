package com.bank.controller;

import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.dto.customer.CustomerResponseSuccess;
import com.bank.service.CustomerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bank/customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;


    @PostMapping("/registrasi")
    public ResponseEntity<CustomerResponseSuccess> saveAnggota(@Valid @RequestBody CustomerRequest request) {
        CustomerResponseSuccess respone = customerService.saveCustomer(request);
        return new ResponseEntity<>(respone, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerResponseSuccess> updateAnggotaById(@RequestHeader("token") String token, @PathVariable("id") Integer id,@RequestBody CustomerRequest request){
        CustomerResponseSuccess respone = customerService.updateCustomerById(token, id, request);
        return ResponseEntity.ok(respone);
    }

    @GetMapping("/infoAccount/{id}")
    public ResponseEntity<CustomerResponse> getInfoAccount(@RequestHeader("token") String token, @PathVariable("id") Integer id)  {
        CustomerResponse response = customerService.getInfoAccount(token, id);
        return ResponseEntity.ok(response);
    }
}
