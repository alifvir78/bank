package com.bank.controller;


import com.bank.dto.transfer.TransferRequest;
import com.bank.dto.transfer.TransferResponse;
import com.bank.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("bank/transfer")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;

    @PostMapping("/{id}")
    public ResponseEntity<TransferResponse> addTransfer (@RequestHeader("token")String token, @PathVariable("id") Integer id,@RequestBody TransferRequest request){
        TransferResponse response =  transferService.addTransfer(id, request,token);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}