package com.bank.controller;

import com.bank.dto.login.LoginRequest;
import com.bank.dto.login.LoginResponse;
import com.bank.service.LoginService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bank/login")
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @PostMapping
    public ResponseEntity<LoginResponse> login(@Valid @RequestBody LoginRequest request){
        LoginResponse respone = loginService.login(request);
        return new ResponseEntity<>(respone, HttpStatus.OK);
    }

}
