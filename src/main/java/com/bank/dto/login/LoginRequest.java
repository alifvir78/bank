package com.bank.dto.login;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class LoginRequest {
    @NotBlank(message = "Silahkan Masukan Username")
    private String username;
    @NotBlank(message = "Silahkan Masukan Password")
    private String password;

}
