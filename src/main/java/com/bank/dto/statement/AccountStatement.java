package com.bank.dto.statement;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class AccountStatement {
    private String nama;
    private String noRekening;
    private BigDecimal saldo;
    private List<StatementList> statementLists;
}
