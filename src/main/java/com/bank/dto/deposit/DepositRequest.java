package com.bank.dto.deposit;

import lombok.*;
import java.math.BigDecimal;

@Data
public class DepositRequest {
    private String metode;
    private BigDecimal nominal;
}
