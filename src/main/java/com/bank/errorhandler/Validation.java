package com.bank.errorhandler;

import com.bank.constant.ConstansMessage;
import com.bank.dto.login.LoginRequest;
import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.entitiy.MetodDeposit;
import com.bank.exception.ErrorHandlerException;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.CustomerRepository;
import com.bank.repository.MethodDepositRepository;
import com.bank.service.TokenService;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class Validation {
    private final TokenService tokenService;
    private final AccountRepository accountRepository;
    private final CustomerRepository customerRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final MethodDepositRepository methodDepositRepository;



    public void validateToken(Integer id,String token) {
        String tokenValid = tokenService.getTokenValid(id);
        if (!tokenValid.equalsIgnoreCase(token)) {
            throw new ErrorHandlerException(ConstansMessage.TOKEN_INVALID_MESSAGE);
        }
    }

    public void validateCustomerNotExists(Integer id) throws CustomerNotExistsException {
        Optional<Account> customer = accountRepository.findById(id);
        if (customer.isEmpty()) {
            throw new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN + id);
        }
    }

    public void validateAccountNotExists(Integer id) {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isEmpty()) {
            throw new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN + id);
        }
    }

    public void validateRekeningAccountNotExists(String noRekening) {
        Optional<Account> rekening = accountRepository.findByNoRekening(noRekening);
        if (rekening.isEmpty()) {
            throw new ErrorHandlerException(ConstansMessage.REKENING_TIDAK_DITEMUKAN);
        }
    }


    public void validateBalance(Account account) {
        if (account.getSaldo().compareTo(BigDecimal.ZERO) < 0) {
            throw new ErrorHandlerException(ConstansMessage.SALDO_KURANG);
        }
    }

    public void validatePin(Account account, String pin) {
        if (!account.getPin().equalsIgnoreCase(pin)) {
            throw new ErrorHandlerException(ConstansMessage.PIN_SALAH);
        }
    }

    public void validateCustomerAlreadyExists(String username, String noHp) {
        boolean existsByUsername = customerRepository.existsByUsername(username);
        boolean existsByNoHp = customerRepository.existsByNoHp(noHp);

        if (existsByUsername && existsByNoHp) {
            throw new ErrorHandlerException(ConstansMessage.USERNAME_NOHP_ALREADY_EXISTS + username +" & "+ noHp);
        } else if (existsByNoHp) {
            throw new ErrorHandlerException(ConstansMessage.NOHP_ALREADY_EXISTS + noHp);
        } else if (existsByUsername) {
            throw new ErrorHandlerException(ConstansMessage.USERNAME_ALREADY_EXISTS + username);
        }

    }


    public void validateTotalTransfer(BigDecimal totalTransfer) {
        if (Objects.isNull(totalTransfer) || (totalTransfer.compareTo(BigDecimal.ZERO) <= 0)) {
            throw new ErrorHandlerException(ConstansMessage.TRANSFER_NOT_NULL);
        }
    }

    public void validateNominalDeposit(BigDecimal nominal) {
        if (Objects.isNull(nominal) || (nominal.compareTo(BigDecimal.ZERO) <= 0)) {
            throw new ErrorHandlerException(ConstansMessage.NOMINAL_NOT_NULL  );
        }
    }


    public void validateTotalWithDraw(BigDecimal totalWithdraw) {
        if (Objects.isNull(totalWithdraw) || (totalWithdraw.compareTo(BigDecimal.ZERO) <= 0)) {
            throw new ErrorHandlerException(ConstansMessage.WITHDRAW_NOT_NULL );
        }
    }

    public void validateMetodeDeposit(String metode){
        Optional<MetodDeposit> keterangan = methodDepositRepository.findByKeterangan(metode);
        if (keterangan.isEmpty()){
            throw new ErrorHandlerException(ConstansMessage.METODE_SALAH);
        }
    }


    public void validateMetodeCannotBlank(String metode) {
        if (StringUtils.isBlank(metode)) {
            throw new ErrorHandlerException(ConstansMessage.METODE_EMPTY);
        }
    }


    public void validatePassword(LoginRequest request){
        Optional<Customer> optionalCustomer = customerRepository.findByUsername(request.getUsername());
        if (!bCryptPasswordEncoder.matches(request.getPassword(), optionalCustomer.get().getPassword())){
            throw new CustomerNotExistsException(ConstansMessage.PASSWORD_SALAH);
        }
    }

    public void validateUsername(LoginRequest request){
        Optional<Customer> optionalCustomer = customerRepository.findByUsername(request.getUsername());
        if(optionalCustomer.isEmpty()){
            throw new CustomerNotExistsException(ConstansMessage.USERNAME_INVALID);
        }
    }



}
