package com.bank.repository;

import com.bank.entitiy.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TransferRepository extends JpaRepository <Transfer, Integer> {

    List<Transfer> findAllByAccountId(@Param("id") Integer id);
}
