package com.bank.exception.customer;

import com.bank.errorhandler.ErrorResponse;

public class CustomerNotExistsException extends RuntimeException{
    public CustomerNotExistsException(String message) {
        super(message);
    }

    public ErrorResponse errorResponseMessage(){
        return new ErrorResponse(getMessage());
    }

}
