package com.bank.exception.rekening;

public class GenerateNorekFailedException extends RuntimeException{
    public GenerateNorekFailedException(String message){
        super(message);
    }
}
