package com.bank.service;


import com.bank.constant.ConstansMessage;
import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.dto.customer.CustomerResponseSuccess;
import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.errorhandler.Validation;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.exception.customer.UsernameAlreadyExistException;
import com.bank.repository.AccountRepository;
import com.bank.repository.CustomerRepository;
import com.bank.util.GenerateNorek;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private Validation validation;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    private CustomerService customerService;


    @Test
    void doPositiveAddCustomer() {
        CustomerRequest request = CustomerRequest.builder()
                .nama("syifa")
                .noHp("086860671625")
                .email("syifa@gmail.com")
                .alamat("sukabumi")
                .username("syifank")
                .pin("000000")
                .password(bCryptPasswordEncoder.encode("123456"))
                .build();

        Customer customer = new Customer();
        customer.setId(1);
        customer.setNama(request.getNama());
        customer.setNoHp(request.getNoHp());
        customer.setEmail(request.getEmail());
        customer.setAlamat(request.getAlamat());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        customer.setLastUpdate(new Date());
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        Account account = new Account();
        account.setId(1);
        account.setCustomer(customer);
        account.setPin(request.getPin());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening(GenerateNorek.generateNomerRekening());
        account.setLastUpdate(new Date());
        when(accountRepository.save(any(Account.class))).thenReturn(account);

        var expect = CustomerResponseSuccess.builder()
                .message(ConstansMessage.REGIST_SUCCESS)
                .id(account.getId())
                .nama(customer.getNama())
                .noHp(customer.getNoHp())
                .email(customer.getEmail())
                .alamat(customer.getAlamat())
                .username(customer.getUsername())
                .nomerRekening(account.getNoRekening())
                .saldo(account.getSaldo())
                .build();

        CustomerResponseSuccess response = customerService.saveCustomer(request);

        Assertions.assertEquals(expect.getMessage(), response.getMessage());
    }

    @Test
    void doNegativeAddCustomer() {
        CustomerRequest request =  CustomerRequest.builder()
                .nama("syifa nabila kadi")
                .alamat("sukabumi")
                .email("syifa@gmail.com")
                .noHp("085860671625")
                .username("syifank")
                .password(bCryptPasswordEncoder.encode("123456"))
                .build();

        doThrow(new UsernameAlreadyExistException(ConstansMessage.USERNAME_NOHP_ALREADY_EXISTS))
                .when(validation).validateCustomerAlreadyExists(request.getUsername(), request.getNoHp());
        Assertions.assertThrows(UsernameAlreadyExistException.class, () ->
                customerService.saveCustomer(request));
    }

    @Test
    void doPositiveUpdateCustomer() throws CustomerNotExistsException {
        Integer id = 1;
        String validToken = "valid_token";

        var customer = new Customer();
        customer.setId(1);
        customer.setNama("nabila");
        customer.setAlamat("bandung");
        customer.setEmail("nabila@gmail.com");
        customer.setNoHp("085219717446");
        customer.setUsername("nabil");
        customer.setPassword(bCryptPasswordEncoder.encode("123456"));

        var akun = new Account();
        akun.setId(1);
        akun.setCustomer(customer);
        akun.setSaldo(BigDecimal.ZERO);
        akun.setPin("123456");

        CustomerRequest request = CustomerRequest.builder()
                .nama("nabila")
                .alamat("bandung")
                .email("nabila@gmail.com")
                .noHp("085219717446")
                .username("nabil")
                .password(bCryptPasswordEncoder.encode("123456"))
                .pin("123456")
                .build();

        validation.validateToken(id, validToken);
     //   Optional<Account> account = Optional.of(akun);

        when(accountRepository.findById(id)).thenReturn(Optional.of(akun));

        // Execution
        CustomerResponseSuccess result = customerService.updateCustomerById(validToken, id, request);
        CustomerResponseSuccess expect = CustomerResponseSuccess.builder()
                .message(ConstansMessage.UPDATE_SUCCESS)
                .id(akun.getId())
                .nama(customer.getNama())
                .noHp(customer.getNoHp())
                .email(customer.getEmail())
                .alamat(customer.getAlamat())
                .username(customer.getUsername())
                .nomerRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .build();

        // Verification
        Assertions.assertNotNull(result);
        Assertions.assertEquals(expect.getMessage(), result.getMessage());
        Assertions.assertEquals(expect.getNama(), result.getNama());
        Assertions.assertEquals(expect.getNoHp(), result.getNoHp());
        Assertions.assertEquals(expect.getEmail(), result.getEmail());
        Assertions.assertEquals(expect.getAlamat(), result.getAlamat());
        Assertions.assertEquals(expect.getUsername(), result.getUsername());
        Assertions.assertEquals(expect.getNomerRekening(), result.getNomerRekening());
        Assertions.assertEquals(expect.getSaldo(), result.getSaldo());
    }

    @Test
    void doNegativeUpdateCustomer() throws  CustomerNotExistsException {
        Integer id = 1;
        String validToken = "valid_token";

        CustomerRequest request = CustomerRequest.builder()
                .nama("nabila")
                .alamat("bandung")
                .email("nabila@gmail.com")
                .noHp("085219717446")
                .username("nabil")
                .password(bCryptPasswordEncoder.encode("123456"))
                .pin("123456")
                .build();

        validation.validateToken(id, validToken);

        when(accountRepository.findById(id)).thenReturn(Optional.empty());

        Assert.assertThrows(CustomerNotExistsException.class, () ->
                customerService.updateCustomerById(validToken, id, request));
    }

    @Test
    void doPosotiveGetInfoAccountById()  {
        Integer id = 1;
        String tokenValid ="token_valid";

        CustomerRequest request = CustomerRequest.builder()
                .nama("syifa")
                .noHp("086860671625")
                .email("syifa@gmail.com")
                .alamat("sukabumi")
                .username("syifank")
                .pin("000000")
                .password(bCryptPasswordEncoder.encode("123456"))
                .build();

        Customer customer = new Customer();
        customer.setId(1);
        customer.setNama(request.getNama());
        customer.setNoHp(request.getNoHp());
        customer.setEmail(request.getEmail());
        customer.setAlamat(request.getAlamat());
        customer.setUsername(request.getUsername());
        customer.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        customer.setLastUpdate(new Date());


        Account account = new Account();
        account.setId(1);
        account.setCustomer(customer);
        account.setPin(request.getPin());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening(GenerateNorek.generateNomerRekening());
        account.setLastUpdate(new Date());

        validation.validateToken(id, tokenValid);
        when(accountRepository.findById(id)).thenReturn(Optional.of(account));

        CustomerResponse response = customerService.getInfoAccount(tokenValid, id);

        Assertions.assertEquals(customer.getNama(), response.getNama());
    }

    @Test
    void doNegativeGetInfoAccountByIdInvalidToken(){
        accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(1)).thenReturn(Optional.empty());
        customerService = new CustomerService(customerRepository, accountRepository, validation, bCryptPasswordEncoder);
        String token = "";

        Assertions.assertThrows(CustomerNotExistsException.class, ()->
                customerService.getInfoAccount(token, 1));
    }
}