package com.bank.util;

import com.bank.dto.login.LoginRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Date;
import java.util.UUID;

@Component
public class JwtToken {

    private static final String SECRET_KEY = String.valueOf(UUID.randomUUID());

    private JwtToken() {
    }

    public static String getToken(@NotNull LoginRequest request) {
        long nowMillis = System.currentTimeMillis();
        var now = new Date(nowMillis);
        var expirationDate = new Date(nowMillis + 15 * 60 * 1000);
        var key = createKey(SECRET_KEY);

        return Jwts.builder()
                .setSubject(String.valueOf(request))
                .setAudience("users")
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(key)
                .compact();
    }

    private static Key createKey(String secret) {
        byte[] encodedKey = secret.getBytes();
        return new SecretKeySpec(encodedKey, 0, encodedKey.length, SignatureAlgorithm.HS512.getJcaName());
    }
}
