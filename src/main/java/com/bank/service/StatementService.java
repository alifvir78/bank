package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.dto.statement.AccountStatement;
import com.bank.dto.statement.StatementList;
import com.bank.entitiy.Account;
import com.bank.entitiy.Deposit;
import com.bank.entitiy.Transfer;
import com.bank.entitiy.Withdraw;
import com.bank.errorhandler.Validation;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.DepositRepository;
import com.bank.repository.TransferRepository;
import com.bank.repository.WithdrawRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatementService {
    private final AccountRepository accountRepository;
    private final DepositRepository depositRepository;
    private final WithdrawRepository withdrawRepository;
    private final TransferRepository transferRepository;
    private final Validation validation;

    public AccountStatement getAll(Integer id, String token){
        validation.validateCustomerNotExists(id);
        validation.validateToken(id, token);

        Optional<Account> account = accountRepository.findById(id);
        Optional<String> nama = account.map(n -> account.get().getCustomer().getNama());
        Optional<String> rekening = account.map(Account::getNoRekening);
        Optional<BigDecimal> saldo = account.map(Account::getSaldo);

        List<Deposit> deposit = depositRepository.findAllByAccountId(id);
        List<Withdraw> withdraw = withdrawRepository.findAllByAccountId(id);
        List<Transfer> transfer = transferRepository.findAllByAccountId(id);

        List<StatementList> transaction = new ArrayList<>();

        List<StatementList> depositState = deposit.stream().map(deposit1 -> {
            var sL = new StatementList();
            sL.setTipeTransaksi("Deposit");
            sL.setNomorTransaksi(deposit1.getNoTransaksi());
            sL.setTanggalTransaksi(deposit1.getDepositDate());
            sL.setKredit(deposit1.getTotalDeposit());
            sL.setDebit(BigDecimal.ZERO);
            sL.setRekeningTujuan("-");

            return sL;
        }).toList();

        List<StatementList> withdrawState = withdraw.stream().map(withdraw1 -> {
            var sL = new StatementList();
            sL.setTipeTransaksi("Withdraw");
            sL.setNomorTransaksi(withdraw1.getNoTransaksi());
            sL.setTanggalTransaksi(withdraw1.getWithdrawDate());
            sL.setKredit(BigDecimal.ZERO);
            sL.setDebit(withdraw1.getTotalWithdraw());
            sL.setRekeningTujuan("-");

            return sL;
        }).toList();

        List<StatementList> transferState = transfer.stream().map(transfer1 -> {
            var sL = new StatementList();
            sL.setTipeTransaksi("Transfer");
            sL.setNomorTransaksi(transfer1.getNoTransaksi());
            sL.setTanggalTransaksi(transfer1.getTransferDate());
            sL.setKredit(BigDecimal.ZERO);
            sL.setDebit(transfer1.getTotalTransfer());
            sL.setRekeningTujuan(transfer1.getRekeningTujuan());

            return sL;
        }).toList();
        transaction.addAll(depositState);
        transaction.addAll(withdrawState);
        transaction.addAll(transferState);

        transaction.sort(Comparator.comparing(StatementList::getTanggalTransaksi));

        return AccountStatement.builder()
                .nama(nama.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .noRekening(rekening.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .saldo(saldo.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .statementLists(transaction)
                .build();
    }

    public AccountStatement getDeposit(Integer id, String token){
        validation.validateCustomerNotExists(id);
        validation.validateToken(id, token);

        Optional<Account> account = accountRepository.findById(id);
        Optional<String> nama = account.map(n -> account.get().getCustomer().getNama());
        Optional<String> rekening = account.map(Account::getNoRekening);
        Optional<BigDecimal> saldo = account.map(Account::getSaldo);

        List<Deposit> deposit = depositRepository.findAllByAccountId(id);

        List<StatementList> depositState = deposit.stream().map(deposit1 -> {
            var sL = new StatementList();
            sL.setTipeTransaksi("Deposit");
            sL.setNomorTransaksi(deposit1.getNoTransaksi());
            sL.setTanggalTransaksi(deposit1.getDepositDate());
            sL.setKredit(deposit1.getTotalDeposit());

            return sL;
        }).toList();
        List<StatementList> transaction = new ArrayList<>(depositState);

        transaction.sort(Comparator.comparing(StatementList::getTanggalTransaksi));

        return AccountStatement.builder()
                .nama(nama.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .noRekening(rekening.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .saldo(saldo.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .statementLists(transaction)
                .build();
    }

    public AccountStatement getWithdraw(Integer id, String token){
        validation.validateCustomerNotExists(id);
        validation.validateToken(id, token);

        Optional<Account> account = accountRepository.findById(id);
        Optional<String> nama = account.map(n -> account.get().getCustomer().getNama());
        Optional<String> rekening = account.map(Account::getNoRekening);
        Optional<BigDecimal> saldo = account.map(Account::getSaldo);

        List<Withdraw> withdraw = withdrawRepository.findAllByAccountId(id);

        List<StatementList> withdrawState = withdraw.stream().map(withdraw1 -> {
            var sL = new StatementList();
            sL.setTipeTransaksi("Withdraw");
            sL.setNomorTransaksi(withdraw1.getNoTransaksi());
            sL.setTanggalTransaksi(withdraw1.getWithdrawDate());
            sL.setDebit(withdraw1.getTotalWithdraw());

            return sL;
        }).toList();
        List<StatementList> transaction = new ArrayList<>(withdrawState);

        transaction.sort(Comparator.comparing(StatementList::getTanggalTransaksi));

        return AccountStatement.builder()
                .nama(nama.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .noRekening(rekening.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .saldo(saldo.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .statementLists(transaction)
                .build();
    }

    public AccountStatement getTransfer(Integer id, String token){
        validation.validateCustomerNotExists(id);
        validation.validateToken(id, token);

        Optional<Account> account = accountRepository.findById(id);
        Optional<String> nama = account.map(n -> account.get().getCustomer().getNama());
        Optional<String> rekening = account.map(Account::getNoRekening);
        Optional<BigDecimal> saldo = account.map(Account::getSaldo);

        List<Transfer> transfer = transferRepository.findAllByAccountId(id);

        List<StatementList> transferState = transfer.stream().map(transfer1 -> {
            var sL = new StatementList();
            sL.setTipeTransaksi("Transfer");
            sL.setNomorTransaksi(transfer1.getNoTransaksi());
            sL.setTanggalTransaksi(transfer1.getTransferDate());
            sL.setDebit(transfer1.getTotalTransfer());
            sL.setRekeningTujuan(transfer1.getRekeningTujuan());

            return sL;
        }).toList();
        List<StatementList> transaction = new ArrayList<>(transferState);

        transaction.sort(Comparator.comparing(StatementList::getTanggalTransaksi));

        return AccountStatement.builder()
                .nama(nama.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .noRekening(rekening.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .saldo(saldo.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN)))
                .statementLists(transaction)
                .build();
    }
}