package com.bank.util;

import com.bank.constant.ConstansMessage;
import com.bank.exception.rekening.GenerateNorekFailedException;
import java.util.UUID;

public class GenerateNorek {

    private GenerateNorek() {
        throw new GenerateNorekFailedException(ConstansMessage.GENERATE_NOREK_GAGAL);
    }

    public static String generateNomerRekening() {
        var norek = UUID.randomUUID().toString().replaceAll("\\D", "").substring(0, 10);
        return String.format("%10s", norek).replace(' ', '0');
    }
}
