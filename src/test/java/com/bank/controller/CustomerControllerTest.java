package com.bank.controller;

import com.bank.constant.ConstansMessage;
import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.dto.customer.CustomerResponseSuccess;
import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.exception.ErrorHandlerException;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.exception.customer.UsernameAlreadyExistException;
import com.bank.service.CustomerService;
import com.bank.util.GenerateNorek;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.math.BigDecimal;
import java.util.Date;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

class CustomerControllerTest {
    @InjectMocks
    private CustomerController customerController;
    @Mock
    private CustomerService customerService;

    @Test
    void doPositifSaveAnggota(){
        var request = CustomerRequest.builder()
                .nama("teest")
                .alamat("testsee")
                .email("test@email")
                .noHp("0982")
                .username("test")
                .password("test")
                .pin("1234")
                .build();

        var customer = new Customer();
        customer.setId(1);
        customer.setNama(request.getNama());
        customer.setNoHp(request.getNoHp());
        customer.setAlamat(request.getAlamat());
        customer.setEmail(request.getEmail());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        customer.setLastUpdate(new Date());

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening(GenerateNorek.generateNomerRekening());
        akun.setSaldo(BigDecimal.ZERO);
        akun.setPin(request.getPin());
        akun.setCustomer(customer);
        akun.setLastUpdate(new Date());

        var response = CustomerResponseSuccess.builder()
                .message(ConstansMessage.REGIST_SUCCESS)
                .id(akun.getId())
                .nama(customer.getNama())
                .noHp(customer.getNoHp())
                .email(customer.getEmail())
                .alamat(customer.getAlamat())
                .username(customer.getUsername())
                .nomerRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .build();
        when(customerService.saveCustomer(request))
                .thenReturn(response);
        ResponseEntity<CustomerResponseSuccess> responseEntity = customerController.saveAnggota(request);
        Assertions.assertSame(HttpStatus.CREATED, responseEntity.getStatusCode());
    }
    @Test
    void doNegatifSaveAnggota(){
        var request = CustomerRequest.builder()
                .nama("teest")
                .alamat("testsee")
                .email("test@email")
                .noHp("0982")
                .username("test")
                .password("test")
                .pin("1234")
                .build();
        doThrow(new UsernameAlreadyExistException(ConstansMessage.USERNAME_NOHP_ALREADY_EXISTS))
                .when(customerService).saveCustomer(request);
        Assertions.assertThrows(UsernameAlreadyExistException.class, ()->
                customerController.saveAnggota(request));
    }
    @Test
    void doPositifUpdate(){
        Integer id = 1;
        String valid = "token";

        var request = CustomerRequest.builder()
                .nama("teest")
                .alamat("testsee")
                .email("test@email")
                .noHp("0982")
                .username("test")
                .password("test")
                .pin("1234")
                .build();

        var customer = new Customer();
        customer.setId(1);
        customer.setNama(request.getNama());
        customer.setNoHp(request.getNoHp());
        customer.setAlamat(request.getAlamat());
        customer.setEmail(request.getEmail());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        customer.setLastUpdate(new Date());

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening(GenerateNorek.generateNomerRekening());
        akun.setSaldo(BigDecimal.ZERO);
        akun.setPin(request.getPin());
        akun.setCustomer(customer);
        akun.setLastUpdate(new Date());

        var response = CustomerResponseSuccess.builder()
                .message(ConstansMessage.UPDATE_SUCCESS)
                .id(akun.getId())
                .nama(customer.getNama())
                .noHp(customer.getNoHp())
                .email(customer.getEmail())
                .alamat(customer.getAlamat())
                .username(customer.getUsername())
                .nomerRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .build();
        when(customerService.updateCustomerById(valid, id, request))
                .thenReturn(response);
        ResponseEntity<CustomerResponseSuccess> responseEntity = customerController.updateAnggotaById(valid, id, request);
        verify(customerService).updateCustomerById(valid, id, request);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
    @Test
    void doNegatifUpdate()  {
        Integer id = 99;
        String invalid = "invalidToken";

        var request = CustomerRequest.builder()
                .email("email.com")
                .build();
        doThrow(new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN))
                .when(customerService).updateCustomerById(invalid, id, request);
        Assertions.assertThrows(CustomerNotExistsException.class, ()->
                customerController.updateAnggotaById(invalid, id, request));
    }
    @Test
    void doPositifGetInfo() {
        Integer id = 1;
        String token = "validToken";

        var response = CustomerResponse.builder()
                .id(1)
                .nama("nico")
                .alamat("test")
                .email("test@emaiil")
                .noHp("021")
                .username("test")
                .nomerRekening("1234")
                .saldo(BigDecimal.ZERO)
                .build();
        when(customerService.getInfoAccount(token, id))
                .thenReturn(response);
        ResponseEntity<CustomerResponse> responseEntity = customerController.getInfoAccount(token, id);
        Assertions.assertSame(HttpStatus.OK, responseEntity.getStatusCode());
    }
    @Test
    void doNegatifGetInfo()  {
        Integer id = 99;
        String token = "invalidToken";

        when(customerService.getInfoAccount(token, id)).thenThrow(ErrorHandlerException.class);
        try {
            ResponseEntity<CustomerResponse> responseEntity = customerController.getInfoAccount(token, id);
            Assertions.assertSame(HttpStatus.OK, responseEntity.getStatusCode());
        }catch (ErrorHandlerException e){
            Assertions.assertEquals("Token tidak valid", ConstansMessage.TOKEN_INVALID_MESSAGE);
        }
    }
}