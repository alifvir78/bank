package com.bank.constant;

public class ConstansMessage {

    private ConstansMessage() {
    }

    public static final String TOKEN_INVALID_MESSAGE = "Token tidak valid";
    public static final String CUSTOMER_TIDAK_DITEMUKAN = "Id Customer berikut tidak ditemukan : ";
    public static final String ACCOUNT_TIDAK_DITEMUKAN = "Account berikut tidak ditemukan : ";
    public static final String REKENING_TIDAK_DITEMUKAN = "No Rekening tujuan tidak ditemukan";
    public static final String SALDO_KURANG = "Saldo Anda Kurang";
    public static final String PIN_SALAH = "Pin yang anda masukan salah";
    public static final String METODE_SALAH = "Metode tidak ditemukan";
    public static final String PASSWORD_SALAH = "Password yang anda masukan salah";
    public static final String GENERATE_NOREK_GAGAL = "Generate Norek Gagal";
    public static final String TRANSFER = "Transfer Dari : ";
    public static final String USERNAME_NOHP_ALREADY_EXISTS = "Username or Nomor HP already exists : ";
    public static final String NOHP_ALREADY_EXISTS = "Nomor HP already exists : ";
    public static final String METODE_EMPTY = "Metode cannot be empty";
    public static final String TRANSFER_NOT_NULL = "Silahkan Isi Total Transfer";
    public static final String NOMINAL_NOT_NULL = "Silahkan Isi Nominal Deposit";
    public static final String WITHDRAW_NOT_NULL = "Silahkan Isi Total WithDraw";
    public static final String USERNAME_ALREADY_EXISTS = "Username already exists : ";
    public static final String REGIST_SUCCESS = "Registrasi Berhasil";
    public static final String UPDATE_SUCCESS = "Data Berhasil Diperbarui, Silahkan Login Kembali";
    public static final String TRANSFER_SUCCESS = "Transfer Berhasil";
    public static final String USERNAME_INVALID = "Username Tidak Ditemukan";

}
