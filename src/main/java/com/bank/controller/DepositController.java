package com.bank.controller;

import com.bank.dto.deposit.DepositRequest;
import com.bank.dto.deposit.DepositResponse;
import com.bank.service.DepositService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/bank/deposit")
@RequiredArgsConstructor
public class DepositController {
    private final DepositService depositService;

    @PostMapping("/{id}")
    public ResponseEntity<DepositResponse> addDeposit(@RequestHeader("token")String token, @PathVariable("id") Integer id,@RequestBody DepositRequest request){
        DepositResponse response = depositService.addDeposit(id, request,token);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
