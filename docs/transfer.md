# Transfer API Spec

## Customer Transfer

Endpoint : POST /api/customers/transfer/{customerId}

Request Body :

```json
{
  "noRekeningTujuan": "987654321",
  "totalTransfer": 100000,
  "pin": "123456"
}
```

Response Body (Success) :

```json
{
  "noRekeningTujuan": "987654321",
  "noTransaksi": "20231214170639191",
  "totalTransfer": 100000,
  "timeTransfer": "14-12-2023 17:06:39"
}
```

Response Body (Failed because the destination account was not found) :

```json
{
  "timestamp": "2023-12-14 10:08:33",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "No Rekening tujuan tidak ditemukan"
}
```

Response Body (Failed because the destination account was not found) :

```json
{
  "timestamp": "2023-12-14 10:08:33",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "No Rekening tujuan tidak ditemukan"
}
```

Response Body (Failed due to insufficient balance) :

```json
{
  "timestamp": "2023-12-14 09:55:13",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "Saldo Anda Kurang"
}
```

Response Body (Failed because of wrong PIN) :

```json
{
  "timestamp": "2023-12-14 09:59:37",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "Pin yang anda masukan salah"
}
```