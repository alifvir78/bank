package com.bank.repository;

import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.entitiy.Deposit;
import com.bank.entitiy.MetodDeposit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class DepositRepoTest {
    @Mock
    private DepositRepository depositRepository;

    public Customer nasabah(){
        var c = new Customer();
        c.setId(1);
        c.setNama("nico");
        c.setEmail("email");
        c.setAlamat("bekasi");
        c.setNoHp("021");
        c.setUsername("nico");
        c.setPassword("nicopas");
        c.setToken("qwerty");
        c.setLastUpdate(new Date());
        return c;
    }

    public Account akun(){
        var account = new Account();
        account.setId(1);
        account.setCustomer(nasabah());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening("123456");
        account.setPin("11");
        account.setLastUpdate(new Date());
        return account;
    }
    @Test
    void doPositifDepositFindByAccountId(){
        var account = akun();

        var deposit = new Deposit();
        deposit.setId(1);
        deposit.setMetodDeposit(new MetodDeposit());
        deposit.setAccount(account);
        deposit.setMetode("atm");
        deposit.setNoTransaksi("4312543");
        deposit.setTotalDeposit(BigDecimal.valueOf(100000));

        List<Deposit> depositList = Collections.singletonList(deposit);

        when(depositRepository.findAllByAccountId(account.getId())).thenReturn(depositList);

        List<Deposit> result = depositRepository.findAllByAccountId(account.getId());

        Assertions.assertEquals(1, result.size());
    }

    @Test
    void doNegatifDepositFindByAccountId(){
        Integer idNotExist = 99;
        when(depositRepository.findAllByAccountId(idNotExist))
                .thenReturn(Collections.emptyList());

        List<Deposit> result = depositRepository.findAllByAccountId(idNotExist);

        Assertions.assertTrue(result.isEmpty());
    }
}
