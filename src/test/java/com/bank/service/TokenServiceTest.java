package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.exception.ErrorHandlerException;
import com.bank.repository.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TokenServiceTest {
    @Mock
    private AccountRepository accountRepository;
    @InjectMocks
    private TokenService tokenService;

    @Test
    void doPositifGetToken(){
        accountRepository = mock(AccountRepository.class);
        var customer = new Customer();
        customer.setId(1);
        customer.setToken("TokenExist");

        var account = new Account();
        account.setId(1);
        account.setCustomer(customer);

        when(accountRepository.findById(1)).thenReturn(Optional.of(account));
        tokenService = new TokenService(accountRepository);
        String expect = tokenService.getTokenValid(1);
        Assertions.assertEquals("TokenExist", expect);
    }
    @Test
    void doNegatifGetToken(){
        accountRepository = mock(AccountRepository.class);
        when(accountRepository.findById(1)).thenReturn(Optional.empty());
        tokenService = new TokenService(accountRepository);

        ErrorHandlerException exception = Assertions.assertThrows(ErrorHandlerException.class, ()-> tokenService.getTokenValid(1));
        Assertions.assertEquals(ConstansMessage.TOKEN_INVALID_MESSAGE, exception.getMessage());
    }
}
