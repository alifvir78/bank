package com.bank.controller;

import com.bank.constant.ConstansMessage;
import com.bank.dto.login.LoginRequest;
import com.bank.dto.login.LoginResponse;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.service.LoginService;
import com.bank.util.JwtToken;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoginControllerTest {
    @InjectMocks
    private LoginController loginController;
    @Mock
    private LoginService loginService;

    @Test
    void doPositifLogin(){
        var request = new LoginRequest();
        request.setUsername("valid");
        request.setPassword("valid");

        var response = LoginResponse.builder()
                .token(JwtToken.getToken(request))
                .build();

        when(loginService.login(request))
                .thenReturn(response);
        ResponseEntity<LoginResponse> responseEntity = loginController.login(request);
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
    @Test
    void doNegatifLogin(){
        var request = new LoginRequest();
        request.setUsername("invalid");
        request.setPassword("invalid");

        doThrow(new CustomerNotExistsException(ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN))
                .when(loginService).login(request);
        Assertions.assertThrows(CustomerNotExistsException.class, ()->
                loginController.login(request));
    }
}
