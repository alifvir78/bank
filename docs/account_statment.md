# Account Statement API Spec

## Get Info Transaction Customer

Endpoint : GET/bank/statement/all/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "saldo": 45000.00,
  "nama" : "nico",
  "noRekening": "no rekening nasabah",
  "statementList":[
    {
      "tipeTransaksi": "Deposit / Withdraw / Transfer",
      "nomorTransaksi": "No Transaksi",
      "tanggalTransaksi": "Tanggal Transaksi",
      "nominal": "Nominal Transaksi",
      "rekeningTujuan" : "simbol (-) untuk Deposit dan Withdraw"
    },
    {
      "tipeTransaksi": "Deposit / Withdraw",
      "nomorTransaksi": "No Transaksi",
      "tanggalTransaksi": "Tanggal Transaksi",
      "nominal": "Nominal Transaksi",
      "rekeningTujuan" : "-"
    },
    {
      "tipeTransaksi": "Transfer",
      "nomorTransaksi": "No Transaksi",
      "tanggalTransaksi": "Tanggal Transaksi",
      "nominal": "Nominal Transaksi",
      "rekeningTujuan" : "Rekening Tujuan"
    }
  ]
}
```

Response Body (Failed) :

```json
{
  "errors" : "Account Tidak Ditemukan"
}
```

Endpoint : GET/bank/statement/deposit/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "saldo": 45000.00,
  "nama" : "nico",
  "noRekening": "no rekening nasabah",
  "statementList":[
    {
      "tipeTransaksi": "Deposit",
      "nomorTransaksi": "No Transaksi",
      "tanggalTransaksi": "Tanggal Transaksi",
      "nominal": "Nominal Transaksi",
      "rekeningTujuan" : "-"
    }
  ]
}
```

Response Body (Failed) :

```json
{
  "errors" : "Account Tidak Ditemukan"
}
```

Endpoint : GET/bank/statement/withdraw/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "saldo": 45000.00,
  "nama" : "nico",
  "noRekening": "no rekening nasabah",
  "statementList":[
    {
      "tipeTransaksi": "Withdraw",
      "nomorTransaksi": "No Transaksi",
      "tanggalTransaksi": "Tanggal Transaksi",
      "nominal": "Nominal Transaksi",
      "rekeningTujuan" : "-"
    }
  ]
}
```

Response Body (Failed) :

```json
{
  "errors" : "Account Tidak Ditemukan"
}
```

Endpoint : GET/bank/statement/transfer/{accountId}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :

```json
{
  "saldo": 45000.00,
  "nama" : "nico",
  "noRekening": "no rekening nasabah",
  "statementList":[
    {
      "tipeTransaksi": "Transfer",
      "nomorTransaksi": "No Transaksi",
      "tanggalTransaksi": "Tanggal Transaksi",
      "nominal": "Nominal Transaksi",
      "rekeningTujuan" : "Rekening Tujuan"
    }
  ]
}
```

Response Body (Failed) :

```json
{
  "errors" : "Account Tidak Ditemukan"
}
```