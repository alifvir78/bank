package com.bank.repository;

import com.bank.entitiy.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Optional<Account> findById(Integer id);
    Optional<Account> findByNoRekening(String noRekening);
}
