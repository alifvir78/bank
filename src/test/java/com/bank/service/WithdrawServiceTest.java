package com.bank.service;

import com.bank.dto.withdraw.WithdrawRequest;
import com.bank.dto.withdraw.WithdrawResponse;
import com.bank.entitiy.Account;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.WithdrawRepository;
import com.bank.errorhandler.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WithdrawServiceTest {

    @Mock
    private WithdrawRepository withdrawRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private Validation validation;

    @InjectMocks
    private WithdrawService withdrawService;

    @Test
    void doPositiveAddWithdraw() {
        String token = "valid_token";
        Integer id = 1;
        WithdrawRequest request = new WithdrawRequest();
        request.setTotalWithdraw(BigDecimal.valueOf(100000));

        Account account = new Account();
        account.setId(1);
        account.setSaldo(BigDecimal.valueOf(1000000).subtract(request.getTotalWithdraw()));
        account.setPin("000000");
        account.setLastUpdate(new Date());

        validation.validateToken(id,token);
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        validation.validateBalance(account);
        validation.validatePin(account, request.getPin());

        String kode = UUID.randomUUID().toString().replaceAll("\\D", "").substring(0, 6);

        WithdrawResponse response = withdrawService.addWithdraw(id, request, token);

        assertEquals(request.getTotalWithdraw(), response.getTotalWithdraw());
        assertNotNull(response.getWithdrawDate());
        assertNotNull(kode);

    }

    @Test
    void doNegativeAddWithdrawInvalidId()  {
        String token = "qwertyuiopgcuyugewrygvcbdcuygdyuvcgeyv";
        Integer id = 1;
        validation.validateToken(id,token);

        Account account = new Account();
        account.setId(99);
        account.setSaldo(BigDecimal.valueOf(1000000).subtract(BigDecimal.valueOf(100000)));
        account.setLastUpdate(new Date());

        when(accountRepository.findById(account.getId())).thenReturn(Optional.empty());
        Assertions.assertThrows(CustomerNotExistsException.class, () ->
                withdrawService.addWithdraw(99, new WithdrawRequest(), token));
    }
}
