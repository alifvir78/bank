package com.bank.repository;

import com.bank.entitiy.Withdraw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface WithdrawRepository extends JpaRepository <Withdraw, Integer> {

    List<Withdraw> findAllByAccountId(@Param("id") Integer id);
}
