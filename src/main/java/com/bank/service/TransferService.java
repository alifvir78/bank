package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.dto.transfer.TransferRequest;
import com.bank.dto.transfer.TransferResponse;
import com.bank.entitiy.Account;
import com.bank.entitiy.Deposit;
import com.bank.entitiy.MetodDeposit;
import com.bank.entitiy.Transfer;
import com.bank.exception.ErrorHandlerException;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.DepositRepository;
import com.bank.repository.MethodDepositRepository;
import com.bank.repository.TransferRepository;
import com.bank.errorhandler.Validation;
import com.bank.util.GenerateNoTransaksi;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TransferService {

    private final TransferRepository transferRepository;
    private final AccountRepository accountRepository;
    private final MethodDepositRepository methodDepositRepository;
    private final Validation validation;
    private final DepositRepository depositRepository;


    @SneakyThrows
    public TransferResponse addTransfer (Integer id, TransferRequest request,String token){
        validation.validateAccountNotExists(id);
        validation.validateToken(id,token);
        Optional<Account> account = accountRepository.findById(id);
        Optional<Account> rekening = accountRepository.findByNoRekening(request.getNoRekeningTujuan());
        validation.validateRekeningAccountNotExists(request.getNoRekeningTujuan());
        validation.validateTotalTransfer(request.getTotalTransfer());

        var akun = account.orElseThrow(() -> new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN)) ;
        akun.setSaldo(akun.getSaldo().subtract(request.getTotalTransfer()));
        akun.setLastUpdate(new Date());

        validation.validateBalance(akun);
        validation.validatePin(akun, request.getPin());

        accountRepository.save(akun);

        var transfer = new Transfer();
        transfer.setNoTransaksi(GenerateNoTransaksi.generateNomorTransaksi());
        transfer.setRekeningTujuan(request.getNoRekeningTujuan());
        transfer.setTotalTransfer(request.getTotalTransfer());
        transfer.setTransferDate(new Date());
        transfer.setLastUpdate(new Date());
        transfer.setAccount(akun);
        transferRepository.save(transfer);

        var rekeningTujuan = rekening.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN)) ;
        rekeningTujuan.setSaldo(rekeningTujuan.getSaldo().add(request.getTotalTransfer()));
        rekeningTujuan.setLastUpdate(new Date());
        accountRepository.save(rekeningTujuan);

        var metode = "transfer";
        Optional<MetodDeposit> metodDepositOptional = methodDepositRepository.findByKeterangan(metode);
        validation.validateMetodeDeposit(metode);
        var metodeDeposit = metodDepositOptional.orElseThrow(()-> new ErrorHandlerException(ConstansMessage.METODE_EMPTY));

        var deposit = new Deposit();
        deposit.setNoTransaksi(transfer.getNoTransaksi());
        deposit.setTotalDeposit(transfer.getTotalTransfer());
        deposit.setDepositDate(transfer.getTransferDate());
        deposit.setLastUpdate(new Date());
        deposit.setAccount(rekeningTujuan);
        deposit.setMetode(ConstansMessage.TRANSFER + akun.getCustomer().getNama());
        deposit.setMetodDeposit(metodeDeposit);
        depositRepository.save(deposit);

        return TransferResponse.builder()
                .message(ConstansMessage.TRANSFER_SUCCESS)
                .noTransaksi(transfer.getNoTransaksi())
                .noRekeningTujuan(transfer.getRekeningTujuan())
                .totalTransfer(transfer.getTotalTransfer())
                .timeTransfer(transfer.getTransferDate())
                .build();
    }
}
