package com.bank.dto.deposit;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DepositResponse {
    private String kodeMetod;
}
