package com.bank.repository;

import com.bank.entitiy.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferRepoTest {
    @Mock
    private TransferRepository transferRepository;

    public Customer nasabah(){
        var c = new Customer();
        c.setId(1);
        c.setNama("nico");
        c.setEmail("email");
        c.setAlamat("bekasi");
        c.setNoHp("021");
        c.setUsername("nico");
        c.setPassword("nicopas");
        c.setToken("qwerty");
        c.setLastUpdate(new Date());
        return c;
    }

    public Account akun(){
        var account = new Account();
        account.setId(1);
        account.setCustomer(nasabah());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening("123456");
        account.setPin("11");
        account.setLastUpdate(new Date());
        return account;
    }
    @Test
    void doPositifTransferFindByAccountId(){
        var account = akun();

        var transfer = new Transfer();
        transfer.setId(1);
        transfer.setRekeningTujuan("dummy rekening tujuan");
        transfer.setAccount(account);
        transfer.setNoTransaksi("4312543");
        transfer.setTotalTransfer(BigDecimal.valueOf(100000));

        List<Transfer> transferList = Collections.singletonList(transfer);

        when(transferRepository.findAllByAccountId(account.getId())).thenReturn(transferList);

        List<Transfer> result = transferRepository.findAllByAccountId(account.getId());

        Assertions.assertEquals(1, result.size());
    }
    @Test
    void doNegatifTransferFindByAccountId(){
        Integer idNotExist = 99;
        when(transferRepository.findAllByAccountId(idNotExist))
                .thenReturn(Collections.emptyList());

        List<Transfer> result = transferRepository.findAllByAccountId(idNotExist);

        Assertions.assertTrue(result.isEmpty());
    }
}
