package com.bank.controller;



import com.bank.dto.transfer.TransferRequest;
import com.bank.dto.transfer.TransferResponse;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.service.TransferService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.math.BigDecimal;
import java.util.Date;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferControllerTest {

    @InjectMocks
    private TransferController transferController;
    @Mock
    private TransferService transferService;

    @Test
    void doPositifTransferAdd(){
        Integer accountId = 1;
        var request = new TransferRequest();
        request.setNoRekeningTujuan("6384917494");
        request.setTotalTransfer(BigDecimal.valueOf(200000));
        request.setPin("1234");

        String token = "valid";

        var response = TransferResponse.builder()
                .noRekeningTujuan("6384917494")
                .timeTransfer(new Date())
                .totalTransfer(BigDecimal.valueOf(200000))
                .build();

        when(transferService.addTransfer(accountId, request, token))
                .thenReturn(response);
        ResponseEntity<TransferResponse> responseEntity = transferController.addTransfer(token, accountId, request);
        Assertions.assertSame(HttpStatus.CREATED, responseEntity.getStatusCode());

    }
    @Test
    void doNegatifTransferAdd(){
        Integer id = 99;
        var request = new TransferRequest();
        request.setNoRekeningTujuan("1323");
        request.setTotalTransfer(BigDecimal.valueOf(50000));
        request.setPin("3231");

        String token = "token";
        doThrow(new CustomerNotExistsException("Customer Not Found"))
                .when(transferService).addTransfer(id, request, token);
        Assertions.assertThrows(CustomerNotExistsException.class, ()->
                transferController.addTransfer(token, id, request));
    }

}
