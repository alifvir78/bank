package com.bank.dto.withdraw;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class WithdrawResponse {
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss" , timezone = "Asia/Jakarta")
    private Date withdrawDate;
    private String noTransaksi;
    private BigDecimal totalWithdraw;
    private String kodeWithdraw;
}