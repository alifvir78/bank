package com.bank.util;

import com.bank.dto.deposit.DepositRequest;
import com.bank.entitiy.Account;
import com.bank.entitiy.MetodDeposit;
import com.bank.errorhandler.Validation;
import com.bank.repository.AccountRepository;
import com.bank.repository.MethodDepositRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KodeDeposit {
    private final AccountRepository accountRepository;
    private final MethodDepositRepository methodDepositRepository;
    private final Validation validation;

    public Optional<String> getRekening(Integer id){
        Optional<Account> account = accountRepository.findById(id);
        return account.map(Account:: getNoRekening);
    }

    public Optional<String> getKodeDepo(String metode){
        Optional<MetodDeposit> keterangan = methodDepositRepository.findByKeterangan(metode);
        validation.validateMetodeDeposit(metode);
        return keterangan.map(MetodDeposit::getKodeMetod);
    }

    public String getKode(Integer id, DepositRequest request){
        Optional<String> metode = getKodeDepo(request.getMetode());
        Optional<String> akun = getRekening(id);

        String md = metode.orElse("");
        String accnt = akun.orElse("");

        return md + accnt;
    }
}
