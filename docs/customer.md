# Customer API Spec

## Register Customer 

Endpoint : POST/bank/customer/registrasi

Request Body :

```json
{
  "nama":"alifvirgiawan",
  "noHp":"098765432",
  "email":"alifvirgiawan@gmail.com",
  "alamat":"tangerang",
  "username":"alifvir78",
  "password":"rahasia",
  "pin":"0909"
}
```

Response Body (Success) :

```json
{
  "id": 1,
  "nama": "alifvirgiawan",
  "noHp": "081223844841",
  "email": "alifvirgiawan@gmail.com",
  "alamat": "tangerang",
  "username": "alifvir78",
  "nomerRekening": "7450468042",
  "saldo": 0
}
```

Response Body (Failed because field is blank):
```json
{
  "errors": [
    "Silahkan Masukan email",
    "Silahkan Masukan alamat",
    "Silahkan Masukan pin",
    "Silahkan Masukan nama",
    "Silahkan Masukan username",
    "Silahkan Masukan noHp",
    "Silahkan Masukan password"
  ]
}
```



## Login Customer

Endpoint : POST/bank/login

Request Body :

```json
{
  "username":"alifvir78",
  "password":"rahasia"
}
```

Response Body (Success) :


```json
{
  "token": "valid_token",
  "lastLogin": "13-12-2023 17:24:30"
}
```

Response Body (Failed because field is blank):

```json
{
  "errors": [
    "Silahkan Masukan Password",
    "Silahkan Masukan Username"
  ]
}
```

## Update Customer

Endpoint : PUT/bank/customer/{id}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :

```json
{
  "username" : "Alif",
  "password" : "new password"
}
```

Response Body (Success) :

```json
{
  "id": 1,
  "nama": "lilo",
  "noHp": "0090",
  "email": "lilo",
  "alamat": "jakarta",
  "username": "lilo",
  "nomerRekening": "0846670425",
  "saldo": 109998
}
```

Response Body (Failed because field is blank):

```json
{
  "errors": [
    "Silahkan Masukan email",
    "Silahkan Masukan alamat",
    "Silahkan Masukan pin",
    "Silahkan Masukan nama",
    "Silahkan Masukan username",
    "Silahkan Masukan noHp",
    "Silahkan Masukan password"
  ]
}
```

