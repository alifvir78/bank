package com.bank.controller;

import com.bank.constant.ConstansMessage;
import com.bank.dto.statement.*;
import com.bank.entitiy.*;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.exportstatement.PdfGenerator;
import com.bank.service.StatementService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.math.BigDecimal;
import java.util.ArrayList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StatementControllerTest {
    @InjectMocks
    private StatementController statementController;
    @Mock
    private StatementService statementService;
    @Mock
    private PdfGenerator pdfGenerator;

    @Test
    void doPositifGetAll(){
        Integer id = 1;
        String token = "validToken";

        var customer = new Customer();
        customer.setId(1);
        customer.setNama("nico");

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening("123123");
        akun.setSaldo(BigDecimal.valueOf(100000));
        akun.setCustomer(customer);

        var accountStatement = AccountStatement.builder()
                .nama(customer.getNama())
                .noRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .statementLists(new ArrayList<>())
                .build();

        statementService = mock(StatementService.class);
        when(statementService.getAll(id, token)).thenReturn(accountStatement);

        statementController = new StatementController(statementService, pdfGenerator);

        ResponseEntity<AccountStatement> response = statementController.getAll(token, id);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response);
    }

    @Test
    void doNegatifGetAll(){
        Integer id = 99;
        String token = "invalidToken";

        when(statementService.getAll(id, token)).thenThrow(CustomerNotExistsException.class);
        try {
            ResponseEntity<AccountStatement> responseEntity = statementController.getAll(token, id);
            Assertions.assertSame(HttpStatus.OK, responseEntity.getStatusCode());
        }catch (CustomerNotExistsException e){
            Assertions.assertEquals("Id Customer berikut tidak ditemukan : ", ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN);
        }

        verify(statementService).getAll(id, token);
        verifyNoMoreInteractions(statementService);
    }
    @Test
    void doPositifGetDeposit(){
        Integer id = 1;
        String token = "validToken";

        var customer = new Customer();
        customer.setId(1);
        customer.setNama("nico");

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening("123123");
        akun.setSaldo(BigDecimal.valueOf(100000));
        akun.setCustomer(customer);

        var accountStatement = AccountStatement.builder()
                .nama(customer.getNama())
                .noRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .statementLists(new ArrayList<>())
                .build();

        statementService = mock(StatementService.class);
        when(statementService.getDeposit(id, token)).thenReturn(accountStatement);

        statementController = new StatementController(statementService, pdfGenerator);

        ResponseEntity<AccountStatement> response = statementController.getDeposit(token, id);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response);
    }
    @SneakyThrows
    @Test
    void doNegatifGetDeposit(){
        Integer id = 99;
        String token = "invalidToken";

        when(statementService.getDeposit(id, token)).thenThrow(CustomerNotExistsException.class);
        try {
            ResponseEntity<AccountStatement> responseEntity = statementController.getDeposit(token, id);
            Assertions.assertSame(HttpStatus.OK, responseEntity.getStatusCode());
        }catch (CustomerNotExistsException e){
            Assertions.assertEquals("Id Customer berikut tidak ditemukan : ", ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN);
        }

        verify(statementService).getDeposit(id, token);
        verifyNoMoreInteractions(statementService);
    }
    @SneakyThrows
    @Test
    void doPositifGetWithdraw(){
        Integer id = 1;
        String token = "validToken";

        var customer = new Customer();
        customer.setId(1);
        customer.setNama("nico");

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening("123123");
        akun.setSaldo(BigDecimal.valueOf(100000));
        akun.setCustomer(customer);

        var accountStatement = AccountStatement.builder()
                .nama(customer.getNama())
                .noRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .statementLists(new ArrayList<>())
                .build();

        statementService = mock(StatementService.class);
        when(statementService.getWithdraw(id, token)).thenReturn(accountStatement);

        statementController = new StatementController(statementService, pdfGenerator);

        ResponseEntity<AccountStatement> response = statementController.getWitdraw(token, id);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response);
    }
    @SneakyThrows
    @Test
    void doNegatifGetWithdraw(){
        Integer id = 99;
        String token = "invalidToken";

        when(statementService.getWithdraw(id, token)).thenThrow(CustomerNotExistsException.class);
        try {
            ResponseEntity<AccountStatement> responseEntity = statementController.getWitdraw(token, id);
            Assertions.assertSame(HttpStatus.OK, responseEntity.getStatusCode());
        }catch (CustomerNotExistsException e){
            Assertions.assertEquals("Id Customer berikut tidak ditemukan : ", ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN);
        }

        verify(statementService).getWithdraw(id, token);
        verifyNoMoreInteractions(statementService);
    }
    @SneakyThrows
    @Test
    void doPositifGetTransfer(){
        Integer id = 1;
        String token = "validToken";

        var customer = new Customer();
        customer.setId(1);
        customer.setNama("nico");

        var akun = new Account();
        akun.setId(1);
        akun.setNoRekening("123123");
        akun.setSaldo(BigDecimal.valueOf(100000));
        akun.setCustomer(customer);

        var accountStatement = AccountStatement.builder()
                .nama(customer.getNama())
                .noRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .statementLists(new ArrayList<>())
                .build();

        statementService = mock(StatementService.class);
        when(statementService.getTransfer(id, token)).thenReturn(accountStatement);

        statementController = new StatementController(statementService, pdfGenerator);

        ResponseEntity<AccountStatement> response = statementController.getTransfer(token, id);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response);
    }
    @SneakyThrows
    @Test
    void doNegatifGetTransfer(){
        Integer id = 99;
        String token = "invalidToken";

        when(statementService.getTransfer(id, token)).thenThrow(CustomerNotExistsException.class);
        try {
            ResponseEntity<AccountStatement> responseEntity = statementController.getTransfer(token, id);
            Assertions.assertSame(HttpStatus.OK, responseEntity.getStatusCode());
        }catch (CustomerNotExistsException e){
            Assertions.assertEquals("Id Customer berikut tidak ditemukan : ", ConstansMessage.CUSTOMER_TIDAK_DITEMUKAN);
        }

        verify(statementService).getTransfer(id, token);
        verifyNoMoreInteractions(statementService);
    }
}