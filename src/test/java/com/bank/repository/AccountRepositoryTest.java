package com.bank.repository;

import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountRepositoryTest {
    @Mock
    private AccountRepository accountRepository;

    public Customer customer(){
        var cust = new Customer();
        cust.setId(1);
        cust.setNama("syifa");
        cust.setEmail("email");
        cust.setAlamat("sukabumi");
        cust.setNoHp("021");
        cust.setUsername("syifnak");
        cust.setPassword("123456");
        cust.setToken("qwerty");
        cust.setLastUpdate(new Date());
        return cust;
    }

    public Account akun(){
        var account = new Account();
        account.setId(1);
        account.setCustomer(customer());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening("123456");
        account.setPin("11");
        account.setLastUpdate(new Date());
        return account;
    }

    @Test
    void doPositiveFindById(){
        Account account = akun();

        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));

        Optional<Account> response = accountRepository.findById(account.getId());

        Assertions.assertTrue(response.isPresent());
        Assertions.assertEquals(account, response.get());
    }

    @Test
    void doNegativeFindById(){
        Integer invalidId = 99;

        when(accountRepository.findById(invalidId)).thenReturn(Optional.empty());

        Optional<Account> response = accountRepository.findById(invalidId);

        Assertions.assertFalse(response.isPresent());
    }

    @Test
    void doPositiveFindByNoRekening(){
        Account account = akun();

        when(accountRepository.findByNoRekening(account.getNoRekening())).thenReturn(Optional.of(account));

        Optional<Account> response = accountRepository.findByNoRekening(account.getNoRekening());

        Assertions.assertTrue(response.isPresent());
        Assertions.assertEquals(account, response.get());
    }

    @Test
    void doNegativeFindByNorekening(){
        String invalidNoRek = "112233";

        when(accountRepository.findByNoRekening(invalidNoRek)).thenReturn(Optional.empty());

        Optional<Account> response = accountRepository.findByNoRekening(invalidNoRek);

        Assertions.assertFalse(response.isPresent());
    }
}
