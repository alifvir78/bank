package com.bank.repository;

import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.entitiy.Withdraw;
import com.bank.util.GenerateNoTransaksi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WithdrawRepoTest {
    @Mock
    private WithdrawRepository withdrawRepository;

    public Customer nasabah(){
        var c = new Customer();
        c.setId(1);
        c.setNama("nico");
        c.setEmail("email");
        c.setAlamat("bekasi");
        c.setNoHp("021");
        c.setUsername("nico");
        c.setPassword("nicopas");
        c.setToken("qwerty");
        c.setLastUpdate(new Date());
        return c;
    }

    public Account akun(){
        var account = new Account();
        account.setId(1);
        account.setCustomer(nasabah());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening("123456");
        account.setPin("11");
        account.setLastUpdate(new Date());
        return account;
    }

    @Test
    void doPositifFindByAccountId(){
        var account = akun();

    var wd = new Withdraw();
    wd.setId(1);
    wd.setAccount(akun());
    wd.setNoTransaksi(GenerateNoTransaksi.generateNomorTransaksi());
    wd.setTotalWithdraw(BigDecimal.valueOf(100000));
    wd.setWithdrawDate(new Date());
    wd.setLastUpdate(new Date());

        List<Withdraw> withdrawList = Collections.singletonList(wd);

        when(withdrawRepository.findAllByAccountId(account.getId())).thenReturn(withdrawList);

        List<Withdraw> result = withdrawRepository.findAllByAccountId(account.getId());

        Assertions.assertEquals(1, result.size());
    }
    @Test
    void doNegatifFindByAccountId(){
        Integer idNotExist = 99;
        when(withdrawRepository.findAllByAccountId(idNotExist))
                .thenReturn(Collections.emptyList());

        List<Withdraw> result = withdrawRepository.findAllByAccountId(idNotExist);

        Assertions.assertTrue(result.isEmpty());
    }
}
