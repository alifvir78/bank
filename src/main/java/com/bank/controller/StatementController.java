package com.bank.controller;

import com.bank.dto.statement.AccountStatement;
import com.bank.exportstatement.PdfGenerator;
import com.bank.service.StatementService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/bank/statement")
@RequiredArgsConstructor
public class StatementController {
    private final StatementService statementService;
    private final PdfGenerator pdfGenerator;

    @GetMapping("/all/{id}")
    public ResponseEntity<AccountStatement> getAll(@RequestHeader("token")String token, @PathVariable("id") Integer id){
        AccountStatement response = statementService.getAll(id,token);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/deposit/{id}")
    public ResponseEntity<AccountStatement> getDeposit(@RequestHeader("token")String token,@PathVariable("id") Integer id){
        AccountStatement response = statementService.getDeposit(id,token);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/withdraw/{id}")
    public ResponseEntity<AccountStatement> getWitdraw(@RequestHeader("token")String token,@PathVariable("id") Integer id){
        AccountStatement response = statementService.getWithdraw(id,token);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/transfer/{id}")
    public ResponseEntity<AccountStatement> getTransfer(@RequestHeader("token")String token,@PathVariable("id") Integer id){
        AccountStatement response = statementService.getTransfer(id,token);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/print/{id}")
    public ResponseEntity<byte[]> generate(@PathVariable Integer id, @RequestHeader("token") String token){
        var accountStatement = statementService.getAll(id, token);
        var pdfBytes = pdfGenerator.generatePdf(accountStatement);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("inline", "statement.pdf");

        return ResponseEntity.ok().headers(headers).body(pdfBytes);
    }
}
