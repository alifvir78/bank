package com.bank.dto.customer;

import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@Builder
public class CustomerRequest {
    @NotBlank(message = "Silahkan Masukan nama")
    private String nama;
    @NotBlank(message = "Silahkan Masukan noHp")
    private String noHp;
    @NotBlank(message = "Silahkan Masukan email")
    private String email;
    @NotBlank(message = "Silahkan Masukan alamat")
    private String alamat;
    @NotBlank(message = "Silahkan Masukan username")
    private String username;
    @NotBlank(message = "Silahkan Masukan password")
    private String password;
    @NotBlank(message = "Silahkan Masukan pin")
    private String pin;


}
