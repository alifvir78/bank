package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.dto.customer.CustomerResponseSuccess;
import com.bank.dto.customer.CustomerRequest;
import com.bank.dto.customer.CustomerResponse;
import com.bank.dto.login.LoginRequest;
import com.bank.entitiy.Account;
import com.bank.entitiy.Customer;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.CustomerRepository;
import com.bank.errorhandler.Validation;
import com.bank.util.GenerateNorek;
import com.bank.util.JwtToken;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    private final AccountRepository accountRepository;
    private final Validation validation;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    public CustomerResponseSuccess saveCustomer(CustomerRequest request) {
        validation.validateCustomerAlreadyExists(request.getUsername(), request.getNoHp());

        var customer = new Customer();
        customer.setNama(request.getNama());
        customer.setNoHp(request.getNoHp());
        customer.setEmail(request.getEmail());
        customer.setAlamat(request.getAlamat());
        customer.setUsername(request.getUsername());
        customer.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        customer.setLastUpdate(new Date());
        customerRepository.save(customer);

        var account = new Account();
        account.setCustomer(customer);
        account.setPin(request.getPin());
        account.setSaldo(BigDecimal.ZERO);
        account.setNoRekening(GenerateNorek.generateNomerRekening());
        account.setLastUpdate(new Date());
        accountRepository.save(account);

        return CustomerResponseSuccess.builder()
                .message(ConstansMessage.REGIST_SUCCESS)
                .id(account.getId())
                .nama(customer.getNama())
                .noHp(customer.getNoHp())
                .email(customer.getEmail())
                .alamat(customer.getAlamat())
                .username(customer.getUsername())
                .nomerRekening(account.getNoRekening())
                .saldo(account.getSaldo())
                .build();
    }


    public CustomerResponseSuccess updateCustomerById(String token, Integer id, CustomerRequest request) {
        validation.validateAccountNotExists(id);
        validation.validateToken(id,token);
        Optional<Account> account = accountRepository.findById(id);
        var akun = account.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN));

        var customer = akun.getCustomer();
        customer.setNama(request.getNama());
        customer.setNoHp(request.getNoHp());
        customer.setEmail(request.getEmail());
        customer.setAlamat(request.getAlamat());
        customer.setUsername(request.getUsername());
        customer.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));
        customer.setLastUpdate(new Date());

        var login = new LoginRequest();
        login.setUsername(customer.getUsername());
        login.setPassword(customer.getPassword());

        customer.setToken(JwtToken.getToken(login));
        customerRepository.save(customer);

        akun.setCustomer(customer);
        akun.setPin(request.getPin());
        accountRepository.save(akun);

        return CustomerResponseSuccess.builder()
                .message(ConstansMessage.UPDATE_SUCCESS)
                .id(akun.getId())
                .nama(customer.getNama())
                .noHp(customer.getNoHp())
                .email(customer.getEmail())
                .alamat(customer.getAlamat())
                .username(customer.getUsername())
                .nomerRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .build();
    }

    public CustomerResponse getInfoAccount(String token, Integer id) {

        validation.validateCustomerNotExists(id);
        validation.validateToken(id,token);
        Optional<Account> account = accountRepository.findById(id);

        var akun = account.orElseThrow(()-> new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN));

        return CustomerResponse.builder()
                .id(akun.getId())
                .nama(akun.getCustomer().getNama())
                .noHp(akun.getCustomer().getNoHp())
                .email(akun.getCustomer().getEmail())
                .alamat(akun.getCustomer().getAlamat())
                .username(akun.getCustomer().getUsername())
                .nomerRekening(akun.getNoRekening())
                .saldo(akun.getSaldo())
                .build();
    }
}
