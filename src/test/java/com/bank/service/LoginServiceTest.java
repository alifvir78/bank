package com.bank.service;

import com.bank.dto.login.LoginRequest;
import com.bank.dto.login.LoginResponse;
import com.bank.entitiy.Customer;
import com.bank.entitiy.Login;
import com.bank.errorhandler.Validation;
import com.bank.repository.CustomerRepository;
import com.bank.repository.LoginRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Optional;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LoginServiceTest {
    @InjectMocks
    private LoginService loginService;
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Mock
    private LoginRepository loginRepository;
    @Mock
    private Validation validation;
    @Mock
    private LoginResponse loginResponse;

    @Test
    void doPositifLogin(){
        var request = new LoginRequest();
        request.setUsername("test");
        request.setPassword("test");

        var customer = new Customer();
        customer.setId(1);
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        customer.setToken("validToken");

        lenient().when(customerRepository.findByUsername(request.getUsername()))
                .thenReturn(Optional.of(customer));
        lenient().when(bCryptPasswordEncoder.matches(request.getPassword(), customer.getPassword()))
                .thenReturn(true);

        var login = new Login();
        login.setId(1);
        login.setLastLogin(new Date());
        login.setCustomer(customer);
        when(loginRepository.save(any(Login.class))).thenReturn(login);

        LoginResponse response = loginService.login(request);
        validation.validateUsername(request);
        verify(customerRepository, times(1)).save(customer);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(customer.getToken(), response.getToken());
    }
    @Test
    void doNegatifLogin(){
        var request = new LoginRequest();
        request.setUsername("invalidTest");
        request.setPassword("invalidTest");

        lenient().when(customerRepository.findByUsername(request.getUsername())).thenReturn(Optional.empty());
        Assertions.assertThrows(NoSuchElementException.class, ()->
                loginService.login(request));
    }
}
