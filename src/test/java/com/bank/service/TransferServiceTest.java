package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.dto.transfer.TransferRequest;
import com.bank.dto.transfer.TransferResponse;
import com.bank.entitiy.*;
import com.bank.errorhandler.Validation;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.DepositRepository;
import com.bank.repository.MethodDepositRepository;
import com.bank.repository.TransferRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransferServiceTest {
    @Mock
    private TransferRepository transferRepository;

    @Mock
    private MethodDepositRepository methodDepositRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private Validation validation;

    @Mock
    private DepositRepository depositRepository;

    @InjectMocks
    private TransferService transferService;

    @Test
    void testAddTransfer() {
        Integer id = 1;
        TransferRequest request = new TransferRequest();
        request.setNoRekeningTujuan("1234567890");
        request.setTotalTransfer(BigDecimal.valueOf(100000));
        String token = "valid_token";

        var account = new Account();
        account.setId(id);
        account.setSaldo(BigDecimal.valueOf(1000000));
        account.setPin("000000");
        account.setLastUpdate(new Date());

        var customer = new Customer();
        customer.setNama("alif");
        account.setCustomer(customer);

        var rekeningTujuan = new Account();
        rekeningTujuan.setNoRekening(request.getNoRekeningTujuan());
        rekeningTujuan.setSaldo(BigDecimal.valueOf(500000));
        rekeningTujuan.setLastUpdate(new Date());

        String noTransaksi = "123";
        var transfer = new Transfer();
        transfer.setNoTransaksi(noTransaksi);
        transfer.setRekeningTujuan(request.getNoRekeningTujuan());
        transfer.setTotalTransfer(request.getTotalTransfer());
        transfer.setTransferDate(new Date());
        transfer.setLastUpdate(new Date());
        transfer.setAccount(account);

        var metode = new MetodDeposit();
        metode.setId(1);
        metode.setKeterangan("transfer");
        metode.setKodeMetod("11");
        validation.validateMetodeDeposit(metode.getKeterangan());
        when(methodDepositRepository.findByKeterangan(metode.getKeterangan())).thenReturn(Optional.of(metode));

        var deposit = new Deposit();
        deposit.setNoTransaksi(transfer.getNoTransaksi());
        deposit.setTotalDeposit(transfer.getTotalTransfer());
        deposit.setDepositDate(transfer.getTransferDate());
        deposit.setLastUpdate(new Date());
        deposit.setAccount(rekeningTujuan);
        deposit.setMetode(ConstansMessage.TRANSFER + account.getCustomer().getNama());
        deposit.setMetodDeposit(metode);

        validation.validateAccountNotExists(id);
        validation.validateToken(id, token);
        when(accountRepository.findById(id)).thenReturn(Optional.of(account));
        when(accountRepository.findByNoRekening(request.getNoRekeningTujuan())).thenReturn(Optional.of(rekeningTujuan));
        validation.validateRekeningAccountNotExists(request.getNoRekeningTujuan());
        validation.validateBalance(account);
        validation.validatePin(account, request.getPin());

        when(transferRepository.save(Mockito.any(Transfer.class))).thenReturn(transfer);
        when(depositRepository.save(Mockito.any(Deposit.class))).thenReturn(deposit);

        TransferService service = new TransferService(transferRepository, accountRepository, methodDepositRepository, validation, depositRepository);
        TransferResponse response = service.addTransfer(id, request, token);

        Assertions.assertEquals("Transfer Berhasil", response.getMessage());
        Assertions.assertEquals(request.getNoRekeningTujuan(), response.getNoRekeningTujuan());
        Assertions.assertEquals(request.getTotalTransfer(), response.getTotalTransfer());
        Assertions.assertNotNull(response.getTimeTransfer());
        verify(accountRepository).save(account);
        verify(transferRepository).save(Mockito.any(Transfer.class));
        verify(depositRepository).save(Mockito.any(Deposit.class));
    }



    @Test
    void testAddWithdrawInvalidId()  {
        String token = "valid_token";
        Integer id = 1;
        validation.validateToken(id,token);

        Account account = new Account();
        account.setId(99);
        account.setSaldo(BigDecimal.valueOf(1000000).subtract(BigDecimal.valueOf(100000)));
        account.setLastUpdate(new Date());

        when(accountRepository.findById(account.getId())).thenReturn(Optional.empty());
        Assertions.assertThrows(CustomerNotExistsException.class, () ->
                transferService.addTransfer(99, new TransferRequest(), token));
    }

}
