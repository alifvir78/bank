# Deposit API Spec

## Customer Deposit

Endpoint : POST /api/customers/deposit/{customerId}

Request Body :

```json
{
  "metode": "Alfamart",
  "nominal": 1000000
}
```

Response Body (Success) :

```json
{
  "kodeMetod": "112123456789"
}
```

Response Body (Failed because the deposit method was not found) :

```json
{
  "timestamp": "2023-12-14 10:29:49",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "Metode tidak ditemukan"
}
```