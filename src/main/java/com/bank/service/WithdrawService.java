package com.bank.service;

import com.bank.constant.ConstansMessage;
import com.bank.dto.withdraw.WithdrawRequest;
import com.bank.dto.withdraw.WithdrawResponse;
import com.bank.entitiy.Account;
import com.bank.entitiy.Withdraw;
import com.bank.exception.customer.CustomerNotExistsException;
import com.bank.repository.AccountRepository;
import com.bank.repository.WithdrawRepository;
import com.bank.errorhandler.Validation;
import com.bank.util.GenerateNoTransaksi;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class WithdrawService {

    private final WithdrawRepository withdrawRepository;
    private final AccountRepository accountRepository;
    private final Validation validation;

    @SneakyThrows
    public WithdrawResponse addWithdraw(Integer id, WithdrawRequest request, String token) {
        Optional<Account> account = accountRepository.findById(id);
        validation.validateCustomerNotExists(id);
        validation.validateToken(id,token);
        validation.validateTotalWithDraw(request.getTotalWithdraw());



        var akun = account.orElseThrow(() -> new CustomerNotExistsException(ConstansMessage.ACCOUNT_TIDAK_DITEMUKAN)) ;
        akun.setSaldo(akun.getSaldo().subtract(request.getTotalWithdraw()));
        validation.validateBalance(akun);
        akun.setLastUpdate(new Date());
        validation.validatePin(akun, request.getPin());
        accountRepository.save(akun);

        var withdraw = new Withdraw();
        withdraw.setNoTransaksi(GenerateNoTransaksi.generateNomorTransaksi());
        withdraw.setTotalWithdraw(request.getTotalWithdraw());
        withdraw.setWithdrawDate(new Date());
        withdraw.setLastUpdate(new Date());
        withdraw.setAccount(akun);
        withdrawRepository.save(withdraw);

        var kode = UUID.randomUUID().toString().replaceAll("\\D", "").substring(0, 6);

        return WithdrawResponse.builder()
                .noTransaksi(withdraw.getNoTransaksi())
                .withdrawDate(withdraw.getWithdrawDate())
                .totalWithdraw(withdraw.getTotalWithdraw())
                .kodeWithdraw(kode)
                .withdrawDate(withdraw.getWithdrawDate())
                .build();
    }
}