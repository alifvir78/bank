package com.bank.service;


import com.bank.constant.ConstansMessage;
import com.bank.entitiy.Account;
import com.bank.exception.ErrorHandlerException;
import com.bank.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final AccountRepository accountRepository;

    public Optional<String> getTokenCust(Integer id){
        Optional<Account> keterangan = accountRepository.findById(id);
        return keterangan.map(c -> keterangan.get().getCustomer().getToken());
    }

    public String getTokenValid(Integer id){
        Optional<String> metode = getTokenCust(id);

        return metode.orElseThrow(()-> new ErrorHandlerException(ConstansMessage.TOKEN_INVALID_MESSAGE));
    }
}
