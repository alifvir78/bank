package com.bank;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest

class BisnisDomainBankApplicationTests {
	@Autowired
	private BisnisDomainBankApplication bisnisDomainBankApplication;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(bisnisDomainBankApplication, "Aplikasi Harus Berhasil Dimuat");
	}

}
